use xand_api_client::Address;

use crate::testing_ports::network_entities::{
    EntityAddress, LimitedAgentEntity, MemberEntity, ValidatorEntity,
};
use crate::Result;

/// Trait for queries against the entity set of a generated network
pub trait EntityQueries {
    /// Returns LimitedAgent
    fn get_limited_agent(&self) -> &LimitedAgentEntity;
    /// Returns the member entity at the given entity index
    fn get_member_by_index(&self, idx: usize) -> Result<&MemberEntity>;
    /// Returns the validator entity at the given entity index
    fn get_validator_by_index(&self, idx: usize) -> Result<&ValidatorEntity>;
    /// Returns an identified entity's xand address
    fn get_xand_address_of_entity(&self, entity: EntityAddress) -> Result<Address>;
}
