use xand_api_client::Address;
use xand_network_generator::contracts::network::metadata::xand_entity_set_summary::{
    LimitedAgentMetadata, MemberMetadata, TrustMetadata, ValidatorMetadata,
};

/// Module which contains contracts and implementations for acting on behalf of puppets on a generated network
pub mod actions;
/// Module which contains contracts and implementations for querying puppets on a generated network
pub mod queries;

/// A structure which represents a participant in a generated network's entity set
#[derive(Clone, Debug)]
pub enum EntityAddress {
    ValidatorIndex(usize),
    MemberIndex(usize),
    XandAddress(Address),
}

#[derive(Clone, Debug)]
pub struct MemberEntity(pub MemberMetadata);

#[derive(Clone, Debug)]
pub struct ValidatorEntity(pub ValidatorMetadata);

#[derive(Clone, Debug)]
pub struct TrustEntity(pub TrustMetadata);

#[derive(Clone, Debug)]
pub struct LimitedAgentEntity(pub LimitedAgentMetadata);
