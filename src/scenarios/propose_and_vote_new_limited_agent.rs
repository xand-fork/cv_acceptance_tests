use async_trait::async_trait;
use xand_api_client::{Address, AdministrativeTransaction, SetLimitedAgentId, XandApiClientTrait};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::Error;

// Default index of member submitting change limited agent proposal
const PROPOSING_MEMBER_INDEX: usize = 0;

const SCENARIO_NAME: &str = "vote-new-limited-agent";

pub struct VoteNewLimitedAgentScenario {
    proposer_entity_address: EntityAddress,
}

impl VoteNewLimitedAgentScenario {
    pub fn default() -> Self {
        Self::new(PROPOSING_MEMBER_INDEX)
    }

    pub fn new(proposer_member_index: usize) -> Self {
        VoteNewLimitedAgentScenario {
            proposer_entity_address: EntityAddress::MemberIndex(proposer_member_index),
        }
    }

    async fn read_limited_agent(
        &self,
        puppeteer: &Puppeteer,
    ) -> Result<Option<Address>, crate::Error> {
        info!(
            "Using {:?} to read current limited agent",
            self.proposer_entity_address
        );
        let limited_agent = puppeteer
            .connect_entity(self.proposer_entity_address.clone())
            .await?
            .get_limited_agent()
            .await?;
        debug!("Current Limited Agent: {:?}", &limited_agent);
        Ok(limited_agent)
    }

    async fn set_new_limited_agent(
        &self,
        new_address: Option<Address>,
        puppeteer: &Puppeteer,
    ) -> ScenarioResult {
        let limited_agent_change_proposal =
            AdministrativeTransaction::SetLimitedAgent(SetLimitedAgentId {
                address: new_address,
            });
        let proposal_id = puppeteer
            .propose(
                self.proposer_entity_address.clone(),
                limited_agent_change_proposal,
            )
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await
    }

    async fn assert_limited_agent(
        &self,
        expected_la: Option<Address>,
        puppeteer: &Puppeteer,
    ) -> ScenarioResult {
        info!(
            "Asserting that limited agent is currently {:?}",
            expected_la
        );
        let current_limited_agent = self.read_limited_agent(puppeteer).await?;
        safe_assert_eq(
            &current_limited_agent,
            &expected_la,
            Error::GeneralTestAssertionFailure(format!(
                "Limited agent was expected to be {:?} but was {:?}",
                expected_la, current_limited_agent,
            )),
        )
    }
}

#[async_trait]
impl Scenario for VoteNewLimitedAgentScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        let puppeteer = Puppeteer::from_default_path()?;
        let original_limited_agent = self.read_limited_agent(&puppeteer).await?;

        self.set_new_limited_agent(None, &puppeteer).await?;
        self.assert_limited_agent(None, &puppeteer).await?;

        self.set_new_limited_agent(original_limited_agent.clone(), &puppeteer)
            .await?;
        self.assert_limited_agent(original_limited_agent, &puppeteer)
            .await?;

        Ok(())
    }
}
