use async_trait::async_trait;
use xand_api_client::{
    AddAuthorityKey, AdministrativeTransaction, RegisterSessionKeys, XandApiClientTrait,
    XandTransaction,
};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::utilities::address_extension::AddressExt;
use crate::Error;

// Index of member submitting proposal and querying chain for membership
const PROPOSING_MEMBER_INDEX: usize = 0;

// Index of member submitting proposal and querying chain for membership
const NEW_VALIDATOR_INDEX: usize = 8;

const SCENARIO_NAME: &str = "vote-in-validator";

pub struct VoteInValidatorScenario {
    proposer_member_index: usize,
    new_validator_index: usize,
}

impl VoteInValidatorScenario {
    pub fn default() -> Self {
        Self::new(PROPOSING_MEMBER_INDEX, NEW_VALIDATOR_INDEX)
    }

    pub fn new(proposer_member_index: usize, new_validator_index: usize) -> Self {
        VoteInValidatorScenario {
            proposer_member_index,
            new_validator_index,
        }
    }
}

#[async_trait]
impl Scenario for VoteInValidatorScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        // Load all entities from config
        let puppeteer = Puppeteer::from_default_path()?;
        let onboarder_puppet_address = EntityAddress::MemberIndex(self.proposer_member_index);

        info!(
            "Using member {} to read entities registered on-chain",
            self.proposer_member_index
        );
        let on_chain_validators = puppeteer
            .connect_entity(onboarder_puppet_address.clone())
            .await?
            .get_authority_keys()
            .await?;
        debug!("On-chain validators: {:?}", &on_chain_validators);

        info!(
            "Asserting validator {} is not found on-chain",
            self.new_validator_index
        );

        let new_validator_addr = puppeteer
            .get_validator_by_index(self.new_validator_index)?
            .as_address();
        safe_assert_eq(
            &on_chain_validators.contains(&new_validator_addr),
            &false,
            Error::GeneralTestAssertionFailure(format!(
                "Found {:?} ({}) on-chain when expected to be unlisted",
                self.new_validator_index, new_validator_addr,
            )),
        )?;

        let add_validator_proposal = AdministrativeTransaction::AddAuthorityKey(AddAuthorityKey {
            account_id: new_validator_addr.clone(),
        });
        let proposal_id = puppeteer
            .propose(onboarder_puppet_address.clone(), add_validator_proposal)
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await?;

        // register session keys
        let txn = XandTransaction::RegisterSessionKeys(RegisterSessionKeys {
            block_production_pubkey: new_validator_addr.clone(),
            block_finalization_pubkey: new_validator_addr.clone(),
        });
        //
        puppeteer
            .connect_entity(EntityAddress::XandAddress(new_validator_addr.clone()))
            .await?
            .submit_transaction(new_validator_addr.clone(), txn)
            .await
            .unwrap();

        info!("Assert that onboarding member can find new validator in chain state.");
        let on_chain_validators = puppeteer
            .connect_entity(onboarder_puppet_address)
            .await?
            .get_authority_keys()
            .await?;
        safe_assert_eq(
            &on_chain_validators.contains(&new_validator_addr),
            &true,
            Error::GeneralTestAssertionFailure(format!(
                "Did not find {:?} ({}) on-chain when expected to be listed",
                self.new_validator_index, new_validator_addr,
            )),
        )?;
        info!("Confirmed new validator {:?}", &new_validator_addr);
        Ok(())
    }
}
