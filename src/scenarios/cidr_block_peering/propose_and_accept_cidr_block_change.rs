use async_trait::async_trait;
use xand_api_client::{
    AdministrativeTransaction, CidrBlock, ProposalStage, RootAllowlistCidrBlock,
    RootRemoveAllowlistCidrBlock, XandApiClientTrait,
};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::utilities::address_extension::AddressExt;
use crate::{
    error::Error::GeneralTestAssertionFailure,
    scenarios::cidr_block_peering::CidrBlockChangeDirection,
};

const SCENARIO_NAME: &str = "cidr-block-change";

/// Structure to contain data and logic for the cidr block change proposal scenario
pub struct CidrProposalChangeScenario {
    proposer_member_index: usize,
    proposal_direction: CidrBlockChangeDirection,
    cidr_member_index: usize,
    cidr_ip: CidrBlock,
}

impl CidrProposalChangeScenario {
    pub fn new(
        proposal_direction: CidrBlockChangeDirection,
        proposer_member_index: usize,
        cidr_member_index: usize,
        cidr_ip_address: [u8; 5],
    ) -> Self {
        let cidr_ip = CidrBlock(cidr_ip_address);
        Self {
            proposer_member_index,
            proposal_direction,
            cidr_member_index,
            cidr_ip,
        }
    }

    async fn assert_proposal_is_accepted(
        &self,
        proposal_id: u32,
        puppeteer: &Puppeteer,
    ) -> ScenarioResult {
        info!(
            "Asserting proposal {} has an \"Accepted\" status",
            proposal_id
        );
        let accepted_proposal = puppeteer
            .connect_entity(EntityAddress::MemberIndex(self.proposer_member_index))
            .await?
            .get_proposal(proposal_id)
            .await?;
        safe_assert_eq(
            &accepted_proposal.status,
            &ProposalStage::Accepted,
            GeneralTestAssertionFailure(format!(
                "Proposal {} was expected to have an accepted status but instead had {:?}",
                proposal_id, accepted_proposal.status
            )),
        )
    }
}

#[async_trait]
impl Scenario for CidrProposalChangeScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        let puppeteer = Puppeteer::from_default_path()?;
        let cidr_account = puppeteer
            .get_member_by_index(self.cidr_member_index)?
            .as_address();
        let proposer_account = puppeteer
            .get_member_by_index(self.proposer_member_index)?
            .as_address();
        let proposer_puppet_address = EntityAddress::XandAddress(proposer_account.clone());
        let proposal = match self.proposal_direction.clone() {
            CidrBlockChangeDirection::Add => {
                AdministrativeTransaction::RootAllowlistCidrBlock(RootAllowlistCidrBlock {
                    account: cidr_account.clone(),
                    cidr_block: self.cidr_ip,
                })
            }
            CidrBlockChangeDirection::Remove => {
                AdministrativeTransaction::RootRemoveAllowlistCidrBlock(
                    RootRemoveAllowlistCidrBlock {
                        account: cidr_account.clone(),
                        cidr_block: self.cidr_ip,
                    },
                )
            }
        };
        let proposal_id = puppeteer
            .propose(proposer_puppet_address.clone(), proposal)
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await?;
        self.assert_proposal_is_accepted(proposal_id, &puppeteer)
            .await?;
        Ok(())
    }
}
