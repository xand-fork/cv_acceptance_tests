use crate::testing_ports::scenario::retrier::Retrier;
use crate::testing_ports::scenario::ScenarioResult;
use crate::{
    puppeteer::Puppeteer,
    testing_ports::{
        network_entities::{actions::EntityActions, queries::EntityQueries, EntityAddress},
        scenario::{safe_assert_eq, Scenario},
    },
    Error, Result,
};
use async_trait::async_trait;
use futures::future::join_all;
use std::ops::Range;
use xand_api_client::{
    AdministrativeTransaction, ProposalStage, SetValidatorEmissionRate, ValidatorEmissionRate,
    XandApiClient, XandApiClientTrait,
};

// Index of member submitting proposal and querying chain for setting
const PROPOSING_MEMBER_INDEX: usize = 0;

// member 0 proposes, and then 1 and 2 approve to make a majority out of 5
const APPROVING_VOTING_MEMBERS_RANGE: Range<usize> = 1..3;
// member 0 proposes and auto-approves, and then 1, 2 and 3 reject
const REJECTING_VOTING_MEMBERS_RANGE: Range<usize> = 1..4;
const VOTING_VALIDATORS_RANGE: Range<usize> = 0..10;

const SCENARIO_NAME: &str = "set-validator-emission-rate";
const NEW_EMISSION_RATE_MINOR_UNITS: u64 = 17;

pub struct SetValidatorEmissionRateScenario {
    proposer_member: EntityAddress,
}

impl SetValidatorEmissionRateScenario {
    pub fn default() -> Self {
        Self::new(PROPOSING_MEMBER_INDEX)
    }

    pub fn new(proposer_member_index: usize) -> Self {
        SetValidatorEmissionRateScenario {
            proposer_member: EntityAddress::MemberIndex(proposer_member_index),
        }
    }

    async fn validate_preconditions(&self, puppeteer: &Puppeteer) -> Result<ValidatorEmissionRate> {
        info!(
            "Using member {:?} to read validator emissions setting",
            self.proposer_member
        );

        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;
        let current_emission_rate = proposer_client.get_validator_emission_rate().await?;

        if current_emission_rate.minor_units_per_emission == NEW_EMISSION_RATE_MINOR_UNITS {
            return Err(Error::GeneralTestAssertionFailure(format!(
                "This test requires that the initial validator emission rate has a \"minor units per emission\" not equal to {}",
                NEW_EMISSION_RATE_MINOR_UNITS,
            )));
        }

        Ok(current_emission_rate)
    }

    async fn validate_unchanged_emission_rate(
        &self,
        puppeteer: &Puppeteer,
        initial_emission_rate: &ValidatorEmissionRate,
    ) -> Result<()> {
        info!(
            "Using member {:?} to read validator emissions setting",
            self.proposer_member
        );

        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;
        let current_emission_rate = proposer_client.get_validator_emission_rate().await?;

        if current_emission_rate != *initial_emission_rate {
            return Err(Error::GeneralTestAssertionFailure(format!(
                "Expected emission rate to match starting value {:?}, but currently is {:?}",
                initial_emission_rate, current_emission_rate,
            )));
        }

        Ok(())
    }

    async fn propose(&self, puppeteer: &Puppeteer) -> Result<u32> {
        info!(
            "Proposing setting validator emission rate to: {} minor units per block",
            NEW_EMISSION_RATE_MINOR_UNITS
        );
        let set_emission_rate_proposal =
            AdministrativeTransaction::SetValidatorEmissionRate(SetValidatorEmissionRate {
                block_quota: 1,
                minor_units_per_emission: NEW_EMISSION_RATE_MINOR_UNITS,
            });
        let proposal_id = puppeteer
            .propose(self.proposer_member.clone(), set_emission_rate_proposal)
            .await?;

        info!("Proposal ID: {}", proposal_id);

        Ok(proposal_id)
    }

    async fn assert_unanimous_validator_approval_is_ignored(
        &self,
        puppeteer: &Puppeteer,
        proposal_id: u32,
    ) -> Result<()> {
        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;

        info!("Voting \"accept\" on behalf of all validators");
        for val_idx in VOTING_VALIDATORS_RANGE {
            puppeteer
                .vote_for_proposal(EntityAddress::ValidatorIndex(val_idx), proposal_id, true)
                .await?;
        }

        let proposal = proposer_client.get_proposal(proposal_id).await?;
        safe_assert_eq(
            &proposal.status,
            &ProposalStage::Proposed,
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to remain in \"proposed\" state, was actually {:?}",
                proposal_id, proposal.status,
            )),
        )?;
        safe_assert_eq(
            &proposal.votes.len(),
            &1,
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to have one vote (from proposer), but was {:?}",
                proposal_id, proposal.votes,
            )),
        )?;
        let proposer_address =
            puppeteer.get_xand_address_of_entity(self.proposer_member.clone())?;
        safe_assert_eq(
            &proposal.votes.get(&proposer_address),
            &Some(&true),
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to have true vote by proposer address {}, but vote set was {:?}",
                proposal_id,
                proposer_address,
                proposal.votes,
            )),
        )?;

        Ok(())
    }

    async fn assert_minimal_member_approval_accepts_proposal(
        &self,
        puppeteer: &Puppeteer,
        proposal_id: u32,
    ) -> Result<()> {
        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;

        info!("Voting \"accept\" on behalf of bare majority of members");
        for mem_idx in APPROVING_VOTING_MEMBERS_RANGE {
            puppeteer
                .vote_for_proposal(EntityAddress::MemberIndex(mem_idx), proposal_id, true)
                .await?;
        }
        let retrier = Retrier::default();
        retrier
            .retry_until_ok(|| Self::assert_proposal_is_accepted(&proposer_client, proposal_id))
            .await?;

        let proposal = proposer_client.get_proposal(proposal_id).await?;
        safe_assert_eq(
            &proposal.votes.len(),
            &3,
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to have three votes (from proposer and two other members), but was {:?}",
                proposal_id,
                proposal.votes,
            )),
        )?;

        let proposer_address =
            puppeteer.get_xand_address_of_entity(self.proposer_member.clone())?;
        safe_assert_eq(
            &proposal.votes.get(&proposer_address),
            &Some(&true),
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to have true vote by proposer address {}, but vote set was {:?}",
                proposal_id,
                proposer_address,
                proposal.votes,
            )),
        )?;

        for mem_idx in APPROVING_VOTING_MEMBERS_RANGE {
            let voter_address =
                puppeteer.get_xand_address_of_entity(EntityAddress::MemberIndex(mem_idx))?;
            safe_assert_eq(
                &proposal.votes.get(&voter_address),
                &Some(&true),
                Error::GeneralTestAssertionFailure(format!(
                    "Expected proposal {} to have true vote by voter address {}, but vote set was {:?}",
                    proposal_id,
                    voter_address,
                    proposal.votes,
                )),
            )?;
        }

        Ok(())
    }

    async fn assert_minimal_member_rejection_rejects_proposal(
        &self,
        puppeteer: &Puppeteer,
        proposal_id: u32,
    ) -> Result<()> {
        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;

        info!("Voting \"reject\" on behalf of bare majority of members");
        let vote_futs = REJECTING_VOTING_MEMBERS_RANGE.map(|mem_idx| {
            puppeteer.vote_for_proposal(EntityAddress::MemberIndex(mem_idx), proposal_id, false)
        });
        join_all(vote_futs)
            .await
            .into_iter()
            .collect::<Result<Vec<()>>>()?;

        let retrier = Retrier::default();
        retrier
            .retry_until_ok(|| Self::assert_proposal_is_rejected(&proposer_client, proposal_id))
            .await
    }

    async fn assert_proposal_is_rejected(
        client: &XandApiClient,
        proposal_id: u32,
    ) -> ScenarioResult {
        let proposal = client.get_proposal(proposal_id).await?;
        safe_assert_eq(
            &proposal.status,
            &ProposalStage::Rejected,
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to be rejected, was actually {:?}",
                proposal_id, proposal.status,
            )),
        )
    }

    async fn assert_proposal_is_accepted(
        client: &XandApiClient,
        proposal_id: u32,
    ) -> ScenarioResult {
        let proposal = client.get_proposal(proposal_id).await?;
        safe_assert_eq(
            &proposal.status,
            &ProposalStage::Accepted,
            Error::GeneralTestAssertionFailure(format!(
                "Expected proposal {} to be accepted, was actually {:?}",
                proposal_id, proposal.status,
            )),
        )
    }

    async fn confirm_updated_emission_rate(&self, puppeteer: &Puppeteer) -> Result<()> {
        info!(
            "Using member {:?} to read validator emissions setting",
            self.proposer_member
        );

        let proposer_client = puppeteer
            .connect_entity(self.proposer_member.clone())
            .await?;
        let current_emission_rate = proposer_client.get_validator_emission_rate().await?;

        if current_emission_rate.block_quota != 1 {
            return Err(Error::GeneralTestAssertionFailure(format!(
                "Expected \"block_quota\" to remain set to 1, but was {}",
                current_emission_rate.block_quota
            )));
        }

        if current_emission_rate.minor_units_per_emission != NEW_EMISSION_RATE_MINOR_UNITS {
            return Err(Error::GeneralTestAssertionFailure(format!(
                "Expected enacted proposal to set \"minor_units_per_emission\" to {}, but was {}",
                NEW_EMISSION_RATE_MINOR_UNITS, current_emission_rate.minor_units_per_emission
            )));
        }

        Ok(())
    }
}

#[async_trait]
impl Scenario for SetValidatorEmissionRateScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> Result<()> {
        let puppeteer = Puppeteer::from_default_path()?;

        let initial_emission_rate = self.validate_preconditions(&puppeteer).await?;

        let rejected_proposal_id = self.propose(&puppeteer).await?;
        self.assert_minimal_member_rejection_rejects_proposal(&puppeteer, rejected_proposal_id)
            .await?;

        self.validate_unchanged_emission_rate(&puppeteer, &initial_emission_rate)
            .await?;

        let accepted_proposal_id = self.propose(&puppeteer).await?;

        self.assert_unanimous_validator_approval_is_ignored(&puppeteer, accepted_proposal_id)
            .await?;
        self.assert_minimal_member_approval_accepts_proposal(&puppeteer, accepted_proposal_id)
            .await?;

        self.confirm_updated_emission_rate(&puppeteer).await?;

        Ok(())
    }
}
