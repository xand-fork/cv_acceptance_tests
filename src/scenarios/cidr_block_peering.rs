// TODO - Included in this test are stubs for expanded testing once our docker compose solution supports peering and changing IPs ADO #5760

use async_trait::async_trait;
use xand_api_client::{Address, CidrBlock, XandApiClientTrait};

use crate::error::Error::GeneralTestAssertionFailure;
use crate::puppeteer::Puppeteer;
use crate::scenarios::cidr_block_peering::propose_and_accept_cidr_block_change::CidrProposalChangeScenario;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::utilities::address_extension::AddressExt;
use crate::Result;

mod propose_and_accept_cidr_block_change;

/// Enum to flag whether a CIDR block proposal scenario should be run with an Add or Remove proposal
#[derive(Clone, Debug)]
pub enum CidrBlockChangeDirection {
    Add,
    Remove,
}

/// Index of member submitting cidr block change proposals during peering scenarios
pub const CIDR_BLOCK_PROPOSING_MEMBER_INDEX: usize = 0;
/// XAND member index to use for cidr block change proposals during peering scenarios
pub const CIDR_XAND_MEMBER_INDEX: usize = 3;
/// IP address for cidr block remove proposals during peering scenarios (Should be whitelisted with the cidr member index initially)
/// This represents 127.28.0.4/32
// pub const CIDR_WHITELISTED_IP_ADDRESS: [u8; 5] = [127, 28, 0, 4, 32];
/// IP address for cidr block add proposals during peering scenarios (Should not be whitelisted with the cidr member index initially)
/// This represents 127.28.0.9/32
pub const CIDR_UNWHITELISTED_IP_ADDRESS: [u8; 5] = [127, 28, 0, 9, 32];
const SCENARIO_NAME: &str = "cidr-block-peering";

/// Structure to contain data and logic for the cidr block peering scenario
pub struct CidrBlockPeeringScenario {
    user_member_index: usize,
    cidr_member_index: usize,
    cidr_ip: CidrBlock,
}

impl CidrBlockPeeringScenario {
    pub fn default() -> Self {
        Self::new(
            CIDR_BLOCK_PROPOSING_MEMBER_INDEX,
            CIDR_XAND_MEMBER_INDEX,
            CIDR_UNWHITELISTED_IP_ADDRESS,
        )
    }

    pub fn new(
        user_member_index: usize,
        cidr_member_index: usize,
        unlisted_cidr_ip_address: [u8; 5],
    ) -> Self {
        let cidr_ip = CidrBlock(unlisted_cidr_ip_address);
        Self {
            user_member_index,
            cidr_member_index,
            cidr_ip,
        }
    }

    async fn assert_expected_peering_results(
        &self,
        expectation: bool,
        puppeteer: &Puppeteer,
    ) -> ScenarioResult {
        let cidr_addr = puppeteer
            .get_member_by_index(self.cidr_member_index)?
            .as_address();
        // info!(
        //     "Checking peering for account {:?} which is expected to be {}",
        //     self.cidr_account.clone(),
        //     expectation.clone()
        // );
        // let check_result = self.check_peering().await?;
        info!(
            "Checking if account {:?} with cidr block {:?} appears on whitelist, which is expected to be {}",
            cidr_addr.clone(),
            self.cidr_ip.clone(),
            expectation.clone()
        );
        let exists_in_whitelist = self.check_whitelist(puppeteer, cidr_addr.clone()).await?;
        safe_assert_eq(
            &exists_in_whitelist,
            &expectation,
            GeneralTestAssertionFailure(format!(
                "Expected Address {:?} Cidr Block {:?} whitelisting to be {}, but it was {}",
                cidr_addr.clone(),
                self.cidr_ip.clone(),
                expectation.clone(),
                exists_in_whitelist,
            )),
        )?;
        // info!(
        //     "Checking ability to take an action with account {:?} which is expected to be {}",
        //     self.cidr_account.clone(),
        //     expectation.clone()
        // );
        // let action_result = self.attempt_arbitrary_action_with_cidr_account().await?;
        Ok(())
    }

    // async fn check_peering(&self) -> Result<bool> {
    //     // TODO ADO #5760
    //     debug!("TODO - Check Peering For Account / IP");
    //     Ok(true)
    // }

    async fn check_whitelist(&self, puppeteer: &Puppeteer, cidr_addr: Address) -> Result<bool> {
        let whitelist = puppeteer
            .connect_entity(EntityAddress::MemberIndex(self.user_member_index))
            .await?
            .get_allowlist()
            .await?;
        debug!("Full whitelist: {:?}", whitelist);
        for (address, cidrblock) in whitelist {
            if address.eq(&cidr_addr) && cidrblock.eq(&self.cidr_ip) {
                info!(
                    "Account {:?} with cidr block {:?} found",
                    cidr_addr.clone(),
                    self.cidr_ip.clone()
                );
                return Ok(true);
            }
        }
        info!(
            "Account {:?} with cidr block {:?} not found",
            cidr_addr.clone(),
            self.cidr_ip.clone()
        );
        Ok(false)
    }

    // async fn attempt_arbitrary_action_with_cidr_account(&self) -> Result<bool> {
    //     // TODO ADO #5760
    //     debug!("TODO - Attempt Action with Whitelisted or Unwhitelisted Account / IP");
    //     Ok(true)
    // }
}

#[async_trait]
impl Scenario for CidrBlockPeeringScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        let puppeteer = Puppeteer::from_default_path()?;
        // STEP ONE: Prove Initial Set Does Not Work
        // TODO: Test peering does not work with chosen cidr addr/account initially ADO #5760
        // TODO: Test account cannot be used to propose an action initially ADO #5760
        self.assert_expected_peering_results(false, &puppeteer)
            .await?;

        // STEP TWO: Add Cidr Block And Prove Set Now Works
        let cidr_add_subscenario = CidrProposalChangeScenario::new(
            CidrBlockChangeDirection::Add,
            self.user_member_index,
            self.cidr_member_index,
            self.cidr_ip.0,
        );
        cidr_add_subscenario.run_scenario().await?;
        // TODO: Test peering works with new cidr whitelisting cidr addr/account ADO #5760
        // TODO: Test account can be used to propose an action ADO #5760
        self.assert_expected_peering_results(true, &puppeteer)
            .await?;

        // STEP THREE: Remove Cidr Block And Prove Set No Longer Works Again
        let cidr_remove_subscenario = CidrProposalChangeScenario::new(
            CidrBlockChangeDirection::Remove,
            self.user_member_index,
            self.cidr_member_index,
            self.cidr_ip.0,
        );
        cidr_remove_subscenario.run_scenario().await?;
        // TODO: Test peering does not work with removed cidr addr/account ADO #5760
        // TODO: Test account cannot be used to propose an action ADO #5760
        self.assert_expected_peering_results(false, &puppeteer)
            .await?;
        Ok(())
    }
}
