use async_trait::async_trait;
use xand_api_client::{
    AdministrativeTransaction, RemoveAuthorityKey, RemoveMember, XandApiClientTrait,
};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::utilities::address_extension::AddressExt;
use crate::Error;

const PROPOSER_MEMBER_INDEX: usize = 0;
const VALIDATOR_TO_VOTE_OUT_INDEX: usize = 0;

const SCENARIO_NAME: &str = "vote-out-validator";

pub struct VoteOutValidatorScenario {
    pub validator_removal_index: usize,
    pub proposer_member_index: usize,
}

impl VoteOutValidatorScenario {
    pub fn default() -> Self {
        Self::new(VALIDATOR_TO_VOTE_OUT_INDEX, PROPOSER_MEMBER_INDEX)
    }

    pub fn new(validator_removal_index: usize, proposer_member_index: usize) -> Self {
        Self {
            validator_removal_index,
            proposer_member_index,
        }
    }
}

#[async_trait]
impl Scenario for VoteOutValidatorScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        // Given a validator on the network
        let puppeteer = Puppeteer::from_default_path()?;
        let proposer_puppet_address = EntityAddress::MemberIndex(self.proposer_member_index);
        let remover_addr = puppeteer
            .get_member_by_index(self.proposer_member_index)?
            .as_address();

        info!(
            "Using member {} to read entities registered on-chain",
            remover_addr
        );
        let on_chain_validators = puppeteer
            .connect_entity(proposer_puppet_address.clone())
            .await?
            .get_authority_keys()
            .await?;
        debug!("On-chain validators: {:?}", &on_chain_validators);

        let validator_to_remove_addr = puppeteer
            .get_validator_by_index(self.validator_removal_index)?
            .as_address();

        // When a proposal is made to remove that validator
        let remove_validator_proposal =
            AdministrativeTransaction::RemoveAuthorityKey(RemoveAuthorityKey {
                account_id: validator_to_remove_addr.clone(),
            });

        let proposal_id = puppeteer
            .propose(proposer_puppet_address.clone(), remove_validator_proposal)
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await?;

        // Then that validator no longer appears as part of the validator set
        info!("Assert that removing member cannot find removed validator in chain state.");
        let on_chain_validators = puppeteer
            .connect_entity(proposer_puppet_address)
            .await?
            .get_authority_keys()
            .await?;
        debug!("On-chain validators: {:?}", &on_chain_validators);
        safe_assert_eq(
            &on_chain_validators.contains(&validator_to_remove_addr),
            &false,
            Error::GeneralTestAssertionFailure(
                format!("Tried to assert validator {:?} was removed, but the validator was found in the list: {:?}", validator_to_remove_addr, on_chain_validators)
            ),
        )?;
        info!("Confirmed removal for {:?}", &validator_to_remove_addr);

        // And can no longer make proposals or vote on things
        info!("Assert that removed validator cannot make a proposal.");

        let remove_member_proposal = AdministrativeTransaction::RemoveMember(RemoveMember {
            address: remover_addr.clone(),
        });

        let proposal_result = puppeteer
            .propose(
                EntityAddress::XandAddress(validator_to_remove_addr.clone()),
                remove_member_proposal.clone(),
            )
            .await;
        match proposal_result {
            Ok(proposal_id) => Err(Error::GeneralTestAssertionFailure(format!(
                "Tried to assert member {:?} could not propose, but found proposal: {}",
                validator_to_remove_addr, proposal_id
            ))),
            Err(e) => {
                match e {
                    Error::InferError(..) => {
                        info!("Confirmed that proposal by removed validator does not exist after attempt");
                        Ok(())
                    }
                    _ => Err(e),
                }
            }
        }
    }
}
