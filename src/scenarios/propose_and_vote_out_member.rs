use async_trait::async_trait;
use xand_api_client::{AdministrativeTransaction, RemoveMember, XandApiClientTrait};

use crate::puppeteer::Puppeteer;
use crate::testing_ports::network_entities::actions::EntityActions;
use crate::testing_ports::network_entities::queries::EntityQueries;
use crate::testing_ports::network_entities::EntityAddress;
use crate::testing_ports::scenario::{safe_assert_eq, Scenario, ScenarioResult};
use crate::utilities::address_extension::AddressExt;
use crate::{Error, Result};

// Index of member submitting proposal and querying chain for membership
const PROPOSING_MEMBER_INDEX: usize = 0;
// Index of member being voted out
const OUTCAST_MEMBER_INDEX: usize = 1;
const SCENARIO_NAME: &str = "vote-out-member";

pub struct VoteOutMemberScenario {
    proposer_member_index: usize,
    outcast_member_index: usize,
}

impl VoteOutMemberScenario {
    pub fn default() -> Self {
        Self::new(PROPOSING_MEMBER_INDEX, OUTCAST_MEMBER_INDEX)
    }

    pub fn new(proposer_member_index: usize, outcast_member_index: usize) -> Self {
        VoteOutMemberScenario {
            proposer_member_index,
            outcast_member_index,
        }
    }

    pub async fn assert_is_on_chain(
        &self,
        member_idx: usize,
        is_expected_on_chain: bool,
        puppeteer: &Puppeteer,
    ) -> Result<()> {
        let member = puppeteer.get_member_by_index(member_idx)?;
        let on_chain_members = puppeteer
            .connect_entity(EntityAddress::MemberIndex(self.proposer_member_index))
            .await?
            .get_members()
            .await?;
        safe_assert_eq(
            &on_chain_members.contains(&member.as_address()),
            &is_expected_on_chain,
            Error::GeneralTestAssertionFailure(format!(
                "Expected member {:?} ({}) to be {}, but assertion failed.",
                self.outcast_member_index,
                member.as_address(),
                if is_expected_on_chain {
                    "on-chain"
                } else {
                    "NOT on-chain"
                },
            )),
        )?;
        Ok(())
    }

    async fn enact_vote(
        &self,
        proposal: AdministrativeTransaction,
        puppeteer: &Puppeteer,
    ) -> Result<()> {
        let proposer_puppet_address = EntityAddress::MemberIndex(self.proposer_member_index);
        let proposal_id = puppeteer
            .propose(proposer_puppet_address.clone(), proposal.clone())
            .await?;
        puppeteer.vote_unanimously(proposal_id, true).await?;
        Ok(())
    }
}

#[async_trait]
impl Scenario for VoteOutMemberScenario {
    fn get_name(&self) -> String {
        SCENARIO_NAME.to_string()
    }

    async fn run_scenario(&self) -> ScenarioResult {
        info!(
            "Using member {} to read entities registered on-chain",
            self.proposer_member_index
        );
        let puppeteer = Puppeteer::from_default_path()?;
        let on_chain_members = puppeteer
            .connect_entity(EntityAddress::MemberIndex(self.proposer_member_index))
            .await?
            .get_members()
            .await?;
        debug!("Initial on-chain members: {:?}", &on_chain_members);

        self.assert_is_on_chain(self.outcast_member_index, true, &puppeteer)
            .await?;

        let outcast_member_addr = puppeteer
            .get_member_by_index(self.outcast_member_index)?
            .as_address();

        self.enact_vote(
            AdministrativeTransaction::RemoveMember(RemoveMember {
                address: outcast_member_addr.clone(),
            }),
            &puppeteer,
        )
        .await?;

        self.assert_is_on_chain(self.outcast_member_index, false, &puppeteer)
            .await?;

        Ok(())
    }
}
