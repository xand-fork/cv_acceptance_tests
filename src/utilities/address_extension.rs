/// Utility extensions to back and forth between XNG Address (string) and XandApiClient's Address
use std::convert::TryInto;

use crate::testing_ports::network_entities::{MemberEntity, ValidatorEntity};
use xand_api_client::Address;
use xand_network_generator::contracts::network::metadata::xand_entity_set_summary::{
    MemberMetadata, TrustMetadata, ValidatorMetadata,
};

pub trait AddressExt: Clone {
    fn as_address(&self) -> Address;
}

impl AddressExt for &MemberMetadata {
    fn as_address(&self) -> Address {
        self.address.clone().try_into().unwrap()
    }
}

impl AddressExt for &ValidatorMetadata {
    fn as_address(&self) -> Address {
        self.address.clone().try_into().unwrap()
    }
}

impl AddressExt for MemberMetadata {
    fn as_address(&self) -> Address {
        self.address.clone().try_into().unwrap()
    }
}

impl AddressExt for ValidatorMetadata {
    fn as_address(&self) -> Address {
        self.address.clone().try_into().unwrap()
    }
}

impl AddressExt for MemberEntity {
    fn as_address(&self) -> Address {
        self.0.as_address()
    }
}

impl AddressExt for ValidatorEntity {
    fn as_address(&self) -> Address {
        self.0.as_address()
    }
}

impl AddressExt for &TrustMetadata {
    fn as_address(&self) -> Address {
        self.address.clone().try_into().unwrap()
    }
}

impl AddressExt for Address {
    fn as_address(&self) -> Address {
        self.clone()
    }
}

impl AddressExt for &Address {
    fn as_address(&self) -> Address {
        (*self).clone()
    }
}
