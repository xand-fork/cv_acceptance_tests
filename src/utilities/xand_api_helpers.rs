use futures::StreamExt;
use xand_api_client::{
    AdministrativeTransaction, Proposal, TransactionStatus, TransactionStatusStream, XandApiClient,
    XandApiClientTrait,
};

use crate::{Error, Result};

pub async fn infer_id(
    api_client: &XandApiClient,
    target_proposal: &AdministrativeTransaction,
) -> Result<u32> {
    info!("Inferring proposal id...");
    let all_proposals = api_client.get_all_proposals().await?;
    let mut matching = all_proposals
        .iter()
        .filter(|p| &p.proposed_action == target_proposal)
        .collect::<Vec<&Proposal>>();

    matching.sort_by_key(|p| p.id);

    match matching.len() {
        0 => Err(crate::Error::InferError(
            "No matching proposals found".into(),
        )),
        1 => Ok(matching.get(0).unwrap().id),
        len => {
            warn!("WARNING: Multiple matching proposals found.");
            Ok(matching.get(len - 1).unwrap().id)
        }
    }
}

pub async fn wait_until_finalized(mut stream: TransactionStatusStream) -> Result<()> {
    while let Some(msg) = stream.next().await {
        trace!("Received update for: {:?}", &msg);

        let msg = msg?;
        match msg.status {
            TransactionStatus::Pending | TransactionStatus::Committed => {}
            TransactionStatus::Unknown | TransactionStatus::Invalid(_) => {
                debug!("{:?}", &msg);
                return Err(Error::TxnFailed(msg.id, msg.status));
            }
            TransactionStatus::Finalized => {
                debug!("{:?}", &msg);
                break;
            }
        }
    }
    Ok(())
}
