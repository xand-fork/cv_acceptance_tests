#![allow(non_snake_case)]
#[macro_use]
extern crate log;

use testing_ports::scenario::ScenarioTestRunner;

use crate::scenarios::{
    AssignNewTrustScenario, CidrBlockPeeringScenario, LimitedAgentProposesAndCannotVote,
    SetValidatorEmissionRateScenario, VoteInMemberScenario, VoteInValidatorScenario,
    VoteNewLimitedAgentScenario, VoteOutMemberScenario, VoteOutValidatorScenario,
};

pub use error::Error;

mod error;
pub mod logging;
pub mod puppeteer;
pub mod scenarios;
pub mod testing_ports;
pub mod utilities;

pub type Result<T, E = error::Error> = std::result::Result<T, E>;

#[tokio::main]
pub async fn main() -> Result<()> {
    crate::logging::init();
    let scenario_tester = ScenarioTestRunner::new(vec![
        Box::new(SetValidatorEmissionRateScenario::default()),
        Box::new(LimitedAgentProposesAndCannotVote::default()),
        Box::new(VoteInMemberScenario::default()),
        Box::new(VoteInValidatorScenario::default()),
        Box::new(VoteOutMemberScenario::default()),
        Box::new(VoteOutValidatorScenario::default()),
        Box::new(CidrBlockPeeringScenario::default()),
        Box::new(AssignNewTrustScenario::default()),
        Box::new(VoteNewLimitedAgentScenario::default()),
    ]);
    scenario_tester.run_all_scenarios().await
}
