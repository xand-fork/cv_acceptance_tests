.PHONY: start stop purge test run


GENERATED_NETWORK_DIR := ./generated

## Variables set for xng network generation
CHAIN_SPECVERSION := "16.3.0"
ZIP_FILENAME := "chain-spec-template.${CHAIN_SPECVERSION}.zip"
CHAINSPEC_TEMPLATES_DIR=./config/chainspec_templates/
XNG_CONFIG_FILEPATH=./config/xng.yaml

##
install-xng:
	cargo install --registry tpfs --version 7.0.0 xand_network_generator

download-chain-spec:
	wget --directory-prefix=${CHAINSPEC_TEMPLATES_DIR} --user=${ARTIFACTORY_USER}  --password=${ARTIFACTORY_PASS} \
		https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${ZIP_FILENAME}

generate:
	xand_network_generator generate  \
        --config ${XNG_CONFIG_FILEPATH} \
        --chainspec-zip ${CHAINSPEC_TEMPLATES_DIR}/${ZIP_FILENAME} \
        --output-dir ${GENERATED_NETWORK_DIR}

start:
	cd $(GENERATED_NETWORK_DIR); docker-compose up -d

stop:
	cd $(GENERATED_NETWORK_DIR); docker-compose down

purge: stop
	cd $(GENERATED_NETWORK_DIR); find ./ -type d -name "chains" -exec sudo rm -rf {} +

test:
	cargo test

run: purge start
	cargo run
